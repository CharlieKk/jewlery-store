import Vue from "vue";
import Vuex from "vuex";
import axios from 'axios'
import createPersistedState from "vuex-persistedstate";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    categories: [],
    subCategories: [],
    products: [],
    currentProduct: {},
    cart: [],
    totalPrice: 0,
  },
  mutations: {
    SET_CATEGORIES(state, categoriesToSet) {
      state.categories = categoriesToSet;
    },
    SET_SUB_CATEGORIES(state, subCategoriesToSet) {
      state.subCategories = subCategoriesToSet;
    },
    SET_PRODUCTS(state, productsToSet) {
      state.products = productsToSet;
    },
    SET_CURRENT_PRODUCT(state, currentProductToSet) {
      state.currentProduct = currentProductToSet;
    },
    ADD_PRODUCT(state, productToAdd) {
      if (state.cart.find(product => product.name === productToAdd.name)) {
        state.cart.find(product => product.name === productToAdd.name).quantity = productToAdd.quantity;
      } else {
        state.cart.push(productToAdd);
      }
    },
    REMOVE_PRODUCT(state, index) {
      state.totalPrice -= state.cart[index].quantity * state.cart[index].price;
      state.cart.splice(index, 1);
    },
    INCREAMENT_QUANTITY_IN_CART(state, index) {
      state.cart[index].quantity++;
      state.totalPrice += state.cart[index].price;
    },
    DECREAMENT_QUANTITY_IN_CART(state, index) {
      if (state.cart[index].quantity > 1) {
        state.cart[index].quantity--;
        state.totalPrice -= state.cart[index].price;
      }
    },
    SET_TOTAL_PRICE(state, totalPriceToSet) {
      state.totalPrice = totalPriceToSet;
    }
  },
  actions: {
    async getAllCategories({ commit }) {
      try {
        const categories = await axios.get("http://localhost:3000/categories")
        commit('SET_CATEGORIES', categories.data);
      }
      catch (error) {
        console.log(error)
      }
    },
    setSubCategories({ commit }, subCategoriesToSet) {
      commit('SET_SUB_CATEGORIES', subCategoriesToSet);
    },
    async getAllProductsBySubCategoryId({ commit }, subCategoryId) {
      try {
        const productsOfCategory = await axios.get("http://localhost:3000/products/" + subCategoryId)
        commit('SET_PRODUCTS', productsOfCategory.data);
      }
      catch (error) {
        console.log("There is error: " + error);
      }
    },
    setCurrentProduct({ commit }, currentproductToSet) {
      commit('SET_CURRENT_PRODUCT', currentproductToSet);
    },
    addToCart({ commit }, productToAdd) {
      commit('ADD_PRODUCT', productToAdd);
    },
    removeProduct({ commit }, index) {
      commit('REMOVE_PRODUCT', index);
    },
    increamentQuantity({ commit }, index) {
      commit('INCREAMENT_QUANTITY_IN_CART', index);
    },
    decreamentQuantity({ commit }, index) {
      commit('DECREAMENT_QUANTITY_IN_CART', index);
    },
    setTotalPrice({ commit },) {
      let totalPriceToSet = 0;
      totalPriceToSet = this.state.cart.reduce( (a,b) => a + b.price * b.quantity, 0)
      commit('SET_TOTAL_PRICE', totalPriceToSet);
    }
  },
  plugins: [createPersistedState({ storage: window.sessionStorage })]
});


