import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import Shop from "./views/Shop";
import SubCategories from "./views/SubCategories";
import Products from "./views/Products";
import Product from "./views/Product";
import Order from "./views/Order";
import LogUp from "./views/LogUp";
import signIn from "./views/SignIn";
import Payment from "./views/Payment";
Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "home",
      component: Home,
    },
    {
      path: "/shop",
      name: "shop",
      component: Shop,
    },
    {
      path: "/subCategories",
      name: "subCategories",
      component: SubCategories,
    },
    {
      path: "/products",
      name: "products",
      component: Products,
    },
    {
      path: "/product",
      name: "product",
      component: Product,
    },
    {
      path: "/order",
      name: "order",
      component: Order,
    },
    {
      path: "/logUp",
      name: "logUp",
      component: LogUp,
    },
    {
      path: "/signIn",
      name: "signIn",
      component: signIn,
    },
    {
      path: "/payment",
      name: "payment",
      component: Payment,
    },
  ],
});
