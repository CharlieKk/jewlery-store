const nodemailer = require('nodemailer');
const hbs = require('nodemailer-express-handlebars');
getToken = (_userId,_role)=>{
  console.log(_userId)
  let token=jsonwebtoken.sign({_Id:_userId,role:_role},process.env.ADMINKEY,{expiresIn:process.env.EXPIURED_TOKEN})
  console.log(token)
  return token;

}

exports.sendMail = async (req) => {
    const transporter = nodemailer.createTransport({
        service: "gmail",
        auth: {
            user: process.env.USER, 
            pass: process.env.PASS 
        }
    });

  

    const mailOptions  = {
        from: process.env.USER,
        to: req.body.mailToSend,  
        subject: 'הזמנתך בוצעה בהצלחה',
        text: 'היי ' + req.body.name + ', הזמנתך בוצעה בהצלחה ואנחנו כבר מתחילים לעבוד עליה, סך הכל עולה ' + req.body.totalPrice + ' שח, תודה רבה',
        template: 'index',
        context: {
          address: req.body.address,
          products: req.body.products,
          isShipment: req.body.isShipment,
          totalpriceWithoutPostage: req.body.totalPrice - 10,
          totalPrice: req.body.totalPrice,
        }
      };

    transporter.sendMail(mailOptions , (error, info) => {
        if (error) {
          console.log(error);
        } else {
          console.log('Email sent: ' + info.response);
        }
      });
}
// const nodemailer = require('nodemailer');
// const hbs = require('nodemailer-express-handlebars');

// exports.sendMail = async (req) => {
//     const transporter = nodemailer.createTransport({
//         service: "yahoo",
//         auth: {
//             user: process.env.USER, 
//             pass: process.env.PASS 
//         }
//     });

//     transporter.use('compile', hbs({
//       viewEngine: {
//         extName: '.handlebars',
//         partialsDir: './views/',
//         layoutsDir: './views/',
//         defaultLayout: false,
//     },
//       viewPath: './views/'
//     }))

//     const mailOptions  = await transporter.sendMail({
//         from: process.env.USER,
//         to: req.body.mailToSend,  
//         subject: 'הזמנתך בוצעה בהצלחה',
//         text: 'היי ' + req.body.name + ', הזמנתך בוצעה בהצלחה ואנחנו כבר מתחילים לעבוד עליה, סך הכל עולה ' + req.body.totalPrice + ' שח, תודה רבה',
//         template: 'index',
//         context: {
//           address: req.body.address,
//           products: req.body.products,
//           isShipment: req.body.isShipment,
//           totalpriceWithoutPostage: req.body.totalPrice - 10,
//           totalPrice: req.body.totalPrice,
//         }
//       });

//     transporter.sendMail(mailOptions , (error, info) => {
//         if (error) {
//           console.log(error);
//         } else {
//           console.log('Email sent: ' + info.response);
//         }
//       });
// }
