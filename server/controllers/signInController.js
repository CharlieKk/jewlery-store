const User = require('../models/user')
const bcrypt=require("bcrypt")
const jsonwebtoken =require("jsonwebtoken")
getToken = (_userId,_role)=>{
    console.log(_userId)
    let token=jsonwebtoken.sign({_Id:_userId,role:_role},process.env.ADMINKEY,{expiresIn:process.env.EXPIURED_TOKEN})
    console.log(token)
    return token;

}

exports.athuUser=(req,res,next)=>{
    let token= req.header("x-api-key")
    if (!token) {
        return res.json({msg:"no token"})
    }
    try {
        let decodeToken=jsonwebtoken.verify(token,process.env.ADMINKEY)
        console.log(decodeToken._Id,"bsdnocbwob")
        req.tokenData=decodeToken
        next();
    } catch (err) {
        res.status(404).json({ message: "no token invalid" });
    }
}




exports.getUsersByToken = async (req,res) => {
    try {
        const user = await User.findOne({_id:req.tokenData._Id});
        res.json(user)
    } catch (err) {
        res.status(404).json({msg:"uincinu"})  
    }
}
exports.getAllUsers = async (res) => {
    try {
        const users = await User.find();
        res.json(users);
    } catch (err) {
        res.json({ message: err });
    }
}





exports.checkUser = async(res, req)=>{
    console.log(req.body.userName,req.body.password)
    const user = await User.findOne({userName:req.body.userName});
    console.log(user)

    if (!user) {
        res.status(201).json({ message: "this user not found" });
    }
    console.log(req.body.password ,user.password)
    const validPassword= await bcrypt.compare(req.body.password ,user.password)
    if (!validPassword) {
        res.status(201).json({ message: "the password worng" });
    }

    let newToken=getToken(user._id,user.role ) 
    res.status(200).json({token:newToken})
      
}
 
 