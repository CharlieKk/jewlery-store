const Product = require('../models/product')

exports.getAllProducts = async (res) => {
    try {
        const products = await Product.find();
        res.json(products);
    } catch (err) {
        res.json({ message: err });
    }
}

exports.getProductsBySubCategoryId = async (res, subCategoryId) => {
    try {
        const products = await Product.find({subCategoryId: subCategoryId});
        res.json(products);
    } catch (err) {
        res.json({ message: err });
    }
}

exports.addProduct = async (res, req) => {
    const ProductToAdd = new Product(
        {
            subCategoryId: req.body.subCategoryId,
            name: req.body.name,
            price: req.body.price,
            discount: req.body.discount,
            description: req.body.description,
            img: req.body.img,
            isOutOfStock: req.body.isOutOfStock
        }
    );

    await ProductToAdd.save().then(data => {
        res.json(data);
    })
}
