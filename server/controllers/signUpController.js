const newMail = require("../controllers/mailTestControler");
const User = require("../models/user");
const bcrypt = require("bcrypt");
const jsonwebtoken = require("jsonwebtoken");
const randomNumber = Math.floor(Math.random() * 9999).toString();

getToken = (_userId, _role, _isActive) => {
  let token = jsonwebtoken.sign(
    { _Id: _userId, role: _role, isActive: _isActive },
    process.env.ADMINKEY,
    { expiresIn: process.env.EXPIURED_TOKEN }
  );
  return token;
};
getTemporaryToken = (_userId, _role, _isActive) => {
  let token = jsonwebtoken.sign(
    { _Id: _userId, role: _role, isActive: _isActive },
    randomNumber,
    { expiresIn: "5min" }
  );
  return token;
};
exports.athuUser = (req, res, next) => {
  let token = req.header("token");
  if (!token) {
    return res.json({ msg: "no token" });
  }
  try {
    let decodeToken = jsonwebtoken.verify(token, process.env.ADMINKEY);
    console.log(decodeToken._Id);
    req.tokenData = decodeToken;
    next();
  } catch (err) {
    res.status(404).json({ message: "no token invalid" });
  }
};
exports.checkIfUserAlreadyExsist = async (res, req) => {
  const users = await User.findOne(req.query);
  console.log(users);
  if (users) {
    res.status(203).json({ message: "this user already exsist 22" });
  } else {
    res.status(200).json({ message: "this user dosent exsist" });
  }
};
exports.addUser = async (res, req) => {
  let userToAdd;
  if (req.body.userName) {
    userToAdd = new User({
      password: req.body.password,
      mail: req.body.mail,
      userName: req.body.userName,
    });
  } else {
    userToAdd = new User({
      password: req.body.password,
      mail: req.body.mail,
      userName: req.body.mail,
    });
  }
  try {
    userToAdd.password = await bcrypt.hash(userToAdd.password, 10);
    await userToAdd.save().then(async (data) => {
      const newUser = await User.findOne({ mail: userToAdd.mail });
      let newTemporaryToken = getTemporaryToken(
        newUser._id,
        newUser.role,
        newUser.isActive
      );
      await newMail.sendMailWitHverificationCode(newUser.mail, randomNumber);
      res.status(200).json({ temporaryToken: newTemporaryToken });
    });
  } catch (error) {
    console.log(error);
    if (error.code == 11000) {
      res.status(400).json({ err: "the user alredy exsist" });
    }
    res.status(400).json({ err: "somthing worng" });
  }
};
exports.changeStatusUser = async (res, req) => {
  let token = req.header("token");
  if (!token) {
    return res.json({ msg: "no token" });
  }
  try {
    let decodeToken = jsonwebtoken.verify(token, req.body.code);
    console.log(decodeToken);
    const user = await User.findOne({ _id: decodeToken._Id });
    await user.update({ $set: { isActive: true } }).then((data) => {
      let newToken = getToken(user._id, user.role, user.isActive);
      res.status(200).json({ token: newToken });
    });
  } catch (err) {
    res.status(404).json({ msg: "Authentication failed" });
  }
};
exports.completionOfDetails = async (res, req) => {
  try {
    const user = await User.findOne({ _id: req.tokenData._Id });
    await user
      .updateOne({
        $set: {
          firstName: req.body.firstName,
          lastName: req.body.lastName,
          address: {
            PostalCode: req.body.PostalCode,
            country: req.body.country,
            city: req.body.city,
            Street: req.body.Street,
            floor: req.body.floor,
            buildingNumber: req.body.buildingNumber,
            ApartmentNumber: req.body.ApartmentNumber,
          },
          phoneNumber: req.body.phoneNumber,
          DateOfBirth: {
            day: req.body.day,
            month: req.body.month,
            year: req.body.year,
          },
          gender: req.body.gender,
        },
      })
      .then((data) => {
        res.status(200).json(data);
      });
  } catch (err) {
    res.status(404).json({ msg: "Authentication failed" });
  }
};
