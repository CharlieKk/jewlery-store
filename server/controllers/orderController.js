const Order = require('../models/order')

exports.getAllOrders = async (res) => {
    try {
        const orders = await Order.find();
        res.json(orders);
    } catch (err) {
        res.json({ message: err });
    }
}

exports.getOrderById = async (res, orderId) => {
    try {
        const order = await Order.findById(orderId);
        res.json(order);
    } catch (err) {
        res.json({ message: err });
    }
}

exports.addOrder = async (res, req) => {
    const orderToAdd = new Order(
        {
            address: req.body.address,
            fullName: req.body.fullName,
            phone: req.body.phone,
            products: req.body.products,
            totalPrice: req.body.totalPrice,
            isShipment: req.body.isShipment,
            comment: req.body.comment
        }
    );

    await orderToAdd.save().then(data => {
        res.json(data);
    })
} 

exports.deleteOrderById = async (res, orderId) => {
    try {
        const removeOrder =  await Order.remove({ _id : orderId});
        res.json(removeOrder);
    } catch (err) {
        res.json({ message: err });
    }
}