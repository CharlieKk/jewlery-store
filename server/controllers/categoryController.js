const Category = require('../models/category')

exports.getAllCategories = async (res) => {
    try {
        const categories = await Category.find();
        res.json(categories);
    } catch (err) {
        res.json({ message: err });
    }
}

exports.getCategoryById = async (res, categoryId) => {
    try {
        const category = await Category.findById(categoryId);
        res.json(category);
    } catch (err) {
        res.json({ message: err });
    }
}

exports.addCategory = async (res, req) => {
    const categoryToAdd = new Category(
        {
            name: req.body.name,
            img: req.body.img,
            products: req.body.products
        }
    );

    await categoryToAdd.save().then(data => {
        res.json(data);
    })
}

exports.deleteCategoryById = async (res, categoryId) => {
    try {
        const removeCategory = await Category.remove({ _id: categoryId });
        res.json(removeCategory);
    } catch (err) {
        res.json({ message: err });
    }
}