const express = require('express')
const router = express.Router()
const mailController = require('../controllers/mailTestControler')

router.post('/', (req,res) => {
    console.log(req.body);
    if (req.body != null) {
        mailController.sendMail(res, req); 
    }
})

module.exports = router;