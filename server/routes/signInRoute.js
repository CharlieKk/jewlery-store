const express = require('express')
const router = express.Router()
const singInController = require('../controllers/signInController')

router.get('/', (req, res) => {
    singInController.getAllUsers(res);
})
router.get('/byToken',singInController.athuUser, (req, res) => {
    singInController.getUsersByToken(req,res);
})

router.post('/signIn', (req,res) => {
    singInController.checkUser(res, req);
})

// ,singInController.athuUser

module.exports = router;