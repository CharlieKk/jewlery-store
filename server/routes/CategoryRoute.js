const express = require('express')
const router = express.Router()
const categoryController = require('../controllers/categoryController')

router.get('/', (req, res) => {
   categoryController.getAllCategories(res);
})

router.get('/:categoryId', (req,res) => {
    categoryController.getCategoryById(res, req.params.categoryId);
})

router.post('/', (req,res) => {
    categoryController.addCategory(res, req);
})

router.delete('/:categoryId', (req, res) => {
    categoryController.deleteCategoryById(res, req.params.categoryId);
})

module.exports = router;