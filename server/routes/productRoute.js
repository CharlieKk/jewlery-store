const express = require('express')
const router = express.Router()
const productsController = require('../controllers/productController')

router.get('/', (req, res) => {
    productsController.getAllProducts(res);
})
router.get('/:subCategoryId', (req, res) => {
    productsController.getProductsBySubCategoryId(res, req.params.subCategoryId);
})

// router.post('/', (req,res) => {
//     productsController.addProduct(res, req);
// })


module.exports = router;