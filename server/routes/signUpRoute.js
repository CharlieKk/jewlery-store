const express = require('express')
const router = express.Router()
const signUpController = require('../controllers/signUpController')


router.get('/checkIfUserAlreadyExsis', (req, res) => {    
    signUpController.checkIfUserAlreadyExsist(res,req);
})

router.post('/signUp', (req,res) => {
    signUpController.addUser(res, req);
})
router.post('/changeStatus', (req,res) => {
    signUpController.changeStatusUser(res, req);
})
router.post('/completionOfDetails',signUpController.athuUser, (req,res) => {
    signUpController.completionOfDetails(res, req);
})





module.exports = router;