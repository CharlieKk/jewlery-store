const express = require('express')
const router = express.Router()
const orderController = require('../controllers/orderController')

router.get('/', (req, res) => {
   orderController.getAllOrders(res);
})

router.get('/:orderId', (req,res) => {
    orderController.getOrderById(res, req.params.orderId);
})

router.post('/', (req,res) => {
    orderController.addOrder(res, req);
})

router.delete('/:orderId', (req, res) => {
    orderController.deleteOrderById(res, req.params.orderId);
})

module.exports = router;