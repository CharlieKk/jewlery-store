const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const productSchema = new Schema({
    subCategoryId: String,
    name: String,
    price: Number,
    discount: Number,
    description: String,
    img: [String],
    isOutOfStock: Boolean
});

module.exports = product = mongoose.model('products', productSchema);

