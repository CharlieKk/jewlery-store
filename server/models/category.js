const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const categorySchema = new Schema({
    name: String,
    img: String,
    subCategories: [ {id: String, name: String, img: String} ]
});

module.exports = category = mongoose.model('categories', categorySchema);

