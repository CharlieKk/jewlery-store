const { number } = require('joi');
const { ObjectId } = require('mongoose');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const userSchema = new Schema({
    RegistrationTime: { type: Date, default: Date.now() },
    firstName: {
        type: String,
        default: "first name"
    },
    lastName: {
        type: String,
        default: "last name"
    },
    mail: String,
    address:{
            PostalCode: {
                type: Number,
                default: 0000
            },
            country:{
                type: String,
                default: "unknow"
            },
            city: {
                type: String,
                default: "unknow"
            },
            Street:{
                type: String,
                default: "unknow"
            } ,
            floor: {
                type: Number,
                default: 000
            },
            buildingNumber:  {
                type: Number,
                default: 000
            },
            ApartmentNumber:  {
                type: Number,
                default: 000
            }
        },
    phoneNumber: {
        type: String,
        default: "unknow"
    },
    userName: {
        type: String,
        default: "unknow"
    },
    password:{
        type: String,
        default: "unknow"
    },
    authenticated: Boolean,
    role: {
        type: String,
        default: "user"
    },
    DateOfBirth: {
            day:  {
                type: Number,
                default: 000
            },
            month:  {
                type: Number,
                default: 000
            },
            year:  {
                type: Number,
                default: 000
            }
    },
    gender: {
        type: String,
        default: "unknow"
    },
    isActive:{
        type:Boolean,
        default: false
    },
    order: [ {
        _id: ObjectId,
        status: String,
        like:Boolean,
        amount: Number
      } ]
   
    
});


module.exports = user = mongoose.model('users', userSchema);
