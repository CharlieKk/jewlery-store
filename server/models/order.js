const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const orderSchema = new Schema({
    date: { type: Date, default: Date.now() },
    address: String,
    fullName: String,
    phone: String,
    products: [{name: String, quantity: Number}],
    totalPrice: Number,
    isShipment: Boolean,
    comment: String
});

module.exports = order = mongoose.model('orders', orderSchema);

