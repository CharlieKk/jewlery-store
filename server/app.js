const express = require('express');
const app = express();
const port = process.env.PORT || 3000;

const mongoose = require('mongoose');
const BodyParser = require("body-parser");

const categoryRoute = require('./routes/categoryRoute');
const productRoute = require('./routes/productRoute');
const orderRoute = require('./routes/orderRoute');
const mailRoute = require('./routes/mailRoute')
const signUpRoute = require('./routes/signUpRoute')
const signInRoute = require('./routes/signInRoute')
// const adminRoute = require('./routes/adminRoute')
// const userRoute = require('./routes/userRoute')
// const logUpRoute = require('./routes/logUpRoute')

require('dotenv/config')

app.use(BodyParser.json());
app.use(BodyParser.urlencoded({ extended: true }));
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader("Access-Control-Allow-Credentials", "true");
    res.setHeader("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
    res.setHeader("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
    next();
});

//Connect to db
mongoose.connect(process.env.DB_CONNECTION, { useNewUrlParser: true }, (err) => {
    if (err) {
        console.log(err);
    } else {
        console.log("Connect to db");
    }
})

app.use('/categories', categoryRoute);
app.use("/products", productRoute)
app.use('/orders', orderRoute);
app.use('/sendMail', mailRoute)
app.use('/signUp', signUpRoute)
app.use('/signIn', signInRoute)
// app.use('/admin', adminRoute)
// app.use('/user', userRoute)
// app.use('/logUp', logUpRoute)

app.listen(port)

